#!/bin/sh
#
#    Author:  Peter Harding <plh@performiq.com>
#
#   Program:  show_db
#
#   Purpose:  Show MySql databases
#
#-------------------------------------------------------------------------------
#
#  MIT License
#   
#  Copyright (C) 1999-2018  Peter Harding
#   
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#  
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#  
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#
#===============================================================================

#set -x

#---- Initialise Globals -------------------------------------------------------

SCRIPT=`basename $0`
USAGE="Usage: $SCRIPT [-d] [-v] [<DBNAME>]"

DEBUG=0; export DEBUG
VERBOSE=0; export VERBOSE
PRINTER=lp; export PRINTER
unset DEBUG
unset VERBOSE

#---- Parse argument list ------------------------------------------------------

while getopts dv OPTS ; do
   case $OPTS in
      d)
         DEBUG=1
         set -x
         ;;

      v)
         VERBOSE=1
         ;;

      \?)
         echo $USAGE
         exit 2
         ;;
   esac
done

shift `expr $OPTIND - 1`

#-------------------------------------------------------------------------------

if [ -f $HOME/etc/mysql.passwd ]; then
   PASSWD_FILE=$HOME/etc/mysql.passwd
elif [ -f /etc/mysql.passwd ] ; then
   PASSWD_FILE=/etc/mysql.passwd
else
   echo "No mysql.passwd file!"
   exit 1
fi

DB_HOST=`cat $PASSWD_FILE | awk -F : '{print $1}'`
DB_USER=`cat $PASSWD_FILE | awk -F : '{print $2}'`
DB_PASSWD=`cat $PASSWD_FILE | awk -F : '{print $3}'`

[ $DEBUG ] && echo "Host $DB_HOST  User $DB_USER  Passwd $DB_PASSWD"

#-------------------------------------------------------------------------------

MYSQL=`which mysql`

if [ -z "$MYSQL" ] ; then
   echo "Cannot find Mysql client"
   exit 1   
fi

#-------------------------------------------------------------------------------

if [ ! -z "$DB_HOST" ] ; then
   HOST="-h $DB_HOST"
else
   DB_HOST=localhost
   HOST=""
fi

if [ ! -z "$DB_PASSWD" ] ; then
   PASSWD="-p$DB_PASSWD"
else
   PASSWD=""
fi

#-------------------------------------------------------------------------------

CREDENTIALS="-u $DB_USER $PASSWD"
CREDENTIALS=""  # Store credentials in ~/.my.cnf

#-------------------------------------------------------------------------------

echo ">>>>> Show databases on $DB_HOST:$DB_NAME <<<<<"

mysql $HOST $CREDENTIALS mysql | expand -20 -30 << XxXxX
show databases;
XxXxX

#===============================================================================
#  Date      Who    Descrption
#-------------------------------------------------------------------------------
#  20020526  plh    Added header & footer
#  20050903  plh    Added support for remote databases
#  20060520  plh    Folded in mysql.passwd in $HOME
#  20060520  plh    Folded in handling of empty password
#  20060520  plh    Added command line options parsing (for debug)
#  20170508  plh    Updated header
#===============================================================================
