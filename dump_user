#!/bin/sh
#
#    Author:  Peter Harding <plh@performiq.com>
#
#   Program:  dump_user
#
#   Purpose:  Export USER table to file
#
#   Copyright (C) 1999-2017  Peter Harding
#             All rights reserved
#
#====================================================================

#---- Initialise Globals ---------------------------------------------

SCRIPT=`basename $0`
USAGE="Usage: $SCRIPT [-d] [-v] [<DBNAME>]"

DEBUG=0; export DEBUG
VERBOSE=0; export VERBOSE
PRINTER=lp; export PRINTER
unset DEBUG
unset VERBOSE

#---- Parse argument list --------------------------------------------

while getopts dv OPTS ; do
   case $OPTS in
      d)
         DEBUG=1
         set -x
         ;;

      v)
         VERBOSE=1
         ;;

      \?)
         echo $USAGE
         exit 2
         ;;
   esac
done

shift `expr $OPTIND - 1`

#--------------------------------------------------------------------

if [ -f $HOME/etc/mysql.passwd ]; then
   PASSWD_FILE=$HOME/etc/mysql.passwd
elif [ -f /etc/mysql.passwd ] ; then
   PASSWD_FILE=/etc/mysql.passwd
else
   echo "No mysql.passwd file!"
   exit 1
fi

DB_HOST=`cat $PASSWD_FILE | awk -F : '{print $1}'`
DB_USER=`cat $PASSWD_FILE | awk -F : '{print $2}'`
DB_PASSWD=`cat $PASSWD_FILE | awk -F : '{print $3}'`

[ $DEBUG ] && echo "Host $DB_HOST  User $DB_USER  Passwd $DB_PASSWD"

#--------------------------------------------------------------------

if [ ! -z "$DB_HOST" ] ; then
   HOST="-h $DB_HOST"
else
   DB_HOST=localhost
   HOST=""
fi

if [ ! -z "$DB_PASSWD" ] ; then
   PASSWD="-p$DB_PASSWD"
else
   PASSWD=""
fi

#--------------------------------------------------------------------

echo ">>>>> Dumping users for $DB_HOST <<<<<"

mysqldump $HOST -u $DB_USER $PASSWD mysql user  > user.imp

#====================================================================
#  Date      Who    Descrption
#--------------------------------------------------------------------
#  20020526  plh    Added header & footer
#  20050903  plh    Added support for remote databases
#  20060520  plh    Folded in mysql.passwd in $HOME
#  20060520  plh    Folded in handling of empty password
#  20060520  plh    Added command line options parsing (for debug)
#  20170508  plh    Updated header
#====================================================================
