MySQL Notes
===========

To add a password use:

  mysqladmin password Xxxxx

Remember to do:

   mysqladmin -u root -pXXXXXX flush-privileges

to flush the old values from the server's cache when changing
a password (or the global one)!


Sat 14 Apr 12:49:15 AEST 2018
=============================

Suppress Password Warning Message
---------------------------------

Try:

  mysql -u root password root -e "statement" > /dev/null

Or...

If your MySQL client/server version is 5.6.x a way to avoid the WARNING
message are using the mysql_config_editor tools:

  mysql_config_editor set --login-path=local --host=localhost --user=username --password

Then you can use in your shell script:

  mysql --login-path=local  -e "statement"

Instead of:

  mysql -u username -p pass -e "statement"


Or use something like:

  mysql --defaults-extra-file=/path/to/config.cnf

or

  mysqldump --defaults-extra-file=/path/to/config.cnf 

Where config.cnf contains:

  [client]
  user = whatever
  password = whatever
  host = whatever

Or add into ~/.my.cnf for individual use.

This allows you to have multiple config files - for different
servers/roles/databases. Using ~/.my.cnf will only allow you to have
one set of configuration (although it may be a useful set of defaults).

If you're on a Debian based distro, and running as root, you could skip
the above and just use /etc/mysql/debian.cnf to get in ... :

  mysql --defaults-extra-file=/etc/mysql/debian.cnf


Adding a User



