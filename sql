#!/bin/sh
#
#    Author:  Peter Harding  <plh@performiq.com.au>
#             Mobile:  0418 375 085
#
#   Program:  sql
#
#   Project:  webdb
#
#   Purpose:  Run some SQL
#
#   Copyright (C) 1999-2017  Peter Harding
#             All rights reserved
#
#=====================================================================

# set -x

#---- Initialise Globals ---------------------------------------------

SCRIPT=`basename $0`
USAGE="Usage: $SCRIPT [-d] [-v] [<DBNAME>]"

DEBUG=0; export DEBUG
VERBOSE=0; export VERBOSE
PRINTER=lp; export PRINTER
unset DEBUG
unset VERBOSE

#---- Parse argument list --------------------------------------------

while getopts dv OPTS ; do
   case $OPTS in
      d)
         DEBUG=1
         set -x
         ;;

      v)
         VERBOSE=1
         ;;

      \?)
         echo $USAGE
         exit 2
         ;;
   esac
done

shift `expr $OPTIND - 1`

#--------------------------------------------------------------------

if [ -f $HOME/etc/mysql.passwd ]; then
   PASSWD_FILE=$HOME/etc/mysql.passwd
elif [ -f /etc/mysql.passwd ] ; then
   PASSWD_FILE=/etc/mysql.passwd
else
   echo "No mysql.passwd file!"
   exit 1
fi

DB_HOST=`cat $PASSWD_FILE | awk -F : '{print $1}'`
DB_USER=`cat $PASSWD_FILE | awk -F : '{print $2}'`
DB_PASSWD=`cat $PASSWD_FILE | awk -F : '{print $3}'`

[ $DEBUG ] && echo "Host $DB_HOST  User $DB_USER  Passwd $DB_PASSWD"

#---------------------------------------------------------------------

FINDIT=`which mysql | wc -l`

if [ $FINDIT -ne 1 ] ; then
   echo "Cannot find Mysql program"
   exit 1   
fi

#---------------------------------------------------------------------

if [ $# -eq 2 ] ; then
   DB_NAME=$1
   SQL=$2
else
   if [ $# -ne 1 ] ; then
      echo "Specify SQL file"
      exit 2
   fi
   if [ -f DBNAME ] ; then
      DB_NAME=`cat DBNAME`
      SQL=$1
   elif [ -f $HOME/DBNAME ] ; then
      DB_NAME=`cat $HOME/DBNAME`
      SQL=$1
   else
      echo "No DBNAME file"
      exit 3
   fi
fi

#--------------------------------------------------------------------

if [ ! -z "$DB_HOST" ] ; then
   HOST="-h $DB_HOST"
else
   DB_HOST=localhost
   HOST=""
fi

if [ ! -z "$DB_PASSWD" ] ; then
   PASSWD="-p$DB_PASSWD"
else
   PASSWD=""
fi

#-------------------------------------------------------------------------------

CREDENTIALS="-u $DB_USER $PASSWD"
CREDENTIALS=""  # Store credentials in ~/.my.cnf

#---------------------------------------------------------------------

echo ">>>>> Running $SQL on $DB_HOST:$DB_NAME <<<<<"

mysql $HOST $CREDENTIALS $DB_NAME < $SQL

#echo $SQL | mysql $HOST $CREDENTIALS $DB_NAME

#=====================================================================
#  Date      Who    Descrption
#---------------------------------------------------------------------
#  20020526  plh    Added header & footer
#  20050903  plh    Added support for remote databases
#  20060520  plh    Folded in mysql.passwd in $HOME
#  20060520  plh    Folded in handling of empty password
#  20060520  plh    Added command line options parsing (for debug)
#=====================================================================

