#!/bin/sh
#
#    Author:  Peter Harding <plh@performiq.com>
#
#   Program:  drop_db
#
#   Purpose:  Drop MySQL database
#
#   Copyright (C) 1999-2017  Peter Harding
#             All rights reserved
#
#-------------------------------------------------------------------------------
#
#  MIT License
#   
#  Copyright (C) 1999-2020  Peter Harding
#   
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#  
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#  
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#
#===============================================================================

#---- Initialise Globals -------------------------------------------------------

SCRIPT=`basename $0`
USAGE="Usage: $SCRIPT [-d] [-v] [<DBNAME>]"

DEBUG=0; export DEBUG
VERBOSE=0; export VERBOSE
PRINTER=lp; export PRINTER
unset DEBUG
unset VERBOSE

#---- Parse argument list ------------------------------------------------------

while getopts dv OPTS ; do
   case $OPTS in
      d)
         DEBUG=1
         set -x
         ;;

      v)
         VERBOSE=1
         ;;

      \?)
         echo $USAGE
         exit 2
         ;;
   esac
done

shift `expr $OPTIND - 1`

#-------------------------------------------------------------------------------

if [ -f $HOME/etc/mysql.passwd ] ; then
   PASSWD_FILE=$HOME/etc/mysql.passwd
elif [ -f /etc/mysql.passwd ] ; then
   PASSWD_FILE=/etc/mysql.passwd
else
   echo "No password file exists"
   exit 1
fi

#-------------------------------------------------------------------------------

DB_HOST=`cat   $PASSWD_FILE | awk -F : '{print $1}'`
DB_USER=`cat   $PASSWD_FILE | awk -F : '{print $2}'`
DB_PASSWD=`cat $PASSWD_FILE | awk -F : '{print $3}'`

echo DB_USER $DB_USER

[ $DEBUG ] && echo "Host $DB_HOST  User $DB_USER  Passwd $DB_PASSWD"

#-------------------------------------------------------------------------------

DIR=`pwd`
TSTAMP=`date "+%Y%m%d"`

#-------------------------------------------------------------------------------

if [ $# -eq 1 ] ; then
   DB_NAME=$1
else
   if [ -f DBNAME ] ; then
      DB_NAME=`cat DBNAME`
   elif [ -f $HOME/DBNAME ] ; then
      DB_NAME=`cat $HOME/DBNAME`
   else
      echo "No DBNAME file"
      exit 2
   fi
fi

#-------------------------------------------------------------------------------

FINDIT=`which mysqladmin | wc -l`

if [ $FINDIT -ne 1 ] ; then
   echo "Cannot find Mysql admin program, \"mysqladmin\""
   exit 1   
fi

#-------------------------------------------------------------------------------

if [ ! -z "$DB_HOST" ] ; then
   HOST="-h $DB_HOST"
else
   DB_HOST=localhost
   HOST=""
fi

if [ ! -z "$DB_PASSWD" ] ; then
   PASSWD="-p$DB_PASSWD"
else
   PASSWD=""
fi

#-------------------------------------------------------------------------------

CREDENTIALS="-u $DB_USER $PASSWD"
CREDENTIALS=""  # Store credentials in ~/.my.cnf

#-------------------------------------------------------------------------------

echo ">>>>> Dropping $DB_HOST:$DB_NAME <<<<<"

if [ ! -f ~/.my.cnf ] ; then

  cat > ~/.my.cnf << XxXxX
[client]
user=$DB_USER
password=$DB_PASSWD

XxXxX

fi

echo ">>>>> Dropping $DB_HOST:$DB_NAME <<<<<"

if [ -f ~/.my.cnf ] ; then
   mysqladmin $HOST -u $DB_USER $PASSWD --force drop $DB_NAME
else
   mysqladmin $HOST --force drop $DB_NAME
fi

#===============================================================================
#  Date      Who    Descrption
#-------------------------------------------------------------------------------
#  20020526  plh    Added header & footer
#  20060207  plh    Modified to use hostname in arguments.
#  20050903  plh    Added support for remote databases
#  20060520  plh    Folded in mysql.passwd in $HOME
#  20060520  plh    Folded in handling of empty password
#  20060520  plh    Added command line options parsing (for debug)
#  20170508  plh    Updated header
#===============================================================================
